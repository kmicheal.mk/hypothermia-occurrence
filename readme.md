# Prediction of time of occurance of hypothemia during general surgical operations.
This repo contains a note book for EDA and models trained.

The dataset used was obtained from here.
https://doi.org/10.13026/czw8-9p62


The extracted copy is avaible in the `vitals-data.csv` file in this same repo.